/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package CompraDePassagem.negocio;

import java.util.List;

/**
 *
 * @author Julio
 */
public interface Cadastro {
    boolean adicionar(Pessoa p);
    Pessoa getPessoaPorNome(String n);
    List<Pessoa> getHomens();
    List<Pessoa> getMulheres();
    List<Pessoa> getTodos();
}
