package CompraDePassagem.factory;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.SQLException;

public class OracleJDBC {

	public static Connection getConnection() {

		System.out.println("-------- Oracle JDBC Connection Testing ------");

		try {

			Class.forName("oracle.jdbc.driver.OracleDriver");

		} catch (ClassNotFoundException e) {

			System.out.println("Where is your Oracle JDBC Driver?");
			e.printStackTrace();
			return null;

		}

		System.out.println("Oracle JDBC Driver Registered!");

		Connection connection = null;
		String hostname = "camburi.pucrs.br";
		String username = "bd104414";
		String password = "bd104414";
		String sid = "facin11g";



		try {

			connection = DriverManager.getConnection(
					"jdbc:oracle:thin:@" + hostname + ":1521:" + sid, username,
					password);

		} catch (SQLException e) {

			System.out.println("Connection Failed! Check output console");
			e.printStackTrace();
			return null;

		}

		if (connection != null) {
			System.out.println("You made it, take control your database now!");
			return connection;
		} else {
			System.out.println("Failed to make connection!");
			return null;
		}
	}
	
	public static String getNome(String cpf) {
		Connection c = getConnection();
		
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT nome FROM users where CPF = ?");
		try {
			PreparedStatement comando = c.prepareStatement(sql.toString());
			comando.setString(1, cpf);
			ResultSet resultado = comando.executeQuery();
			resultado.next();
			return resultado.getString("nome");
		} catch (SQLException e) {
			return null;
		}

	}

}