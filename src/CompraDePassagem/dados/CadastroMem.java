
package CompraDePassagem.dados;

import java.util.ArrayList;
import java.util.List;

import CompraDePassagem.negocio.Cadastro;
import CompraDePassagem.negocio.Pessoa;

/**
 *
 * @author Julio
 */
public class CadastroMem implements Cadastro {
    private List<Pessoa> pessoas;

    public CadastroMem() {
        pessoas = new ArrayList<Pessoa>();
    }

    public boolean adicionar(Pessoa p) {
        return pessoas.add(p);
    }

    public List<Pessoa> getHomens() {
        List<Pessoa> tmp = new ArrayList<Pessoa>();
        for (Pessoa p : pessoas) {
            if (p.getSexo() == 'M') {
                tmp.add(p);
            }
        }
        return tmp;
    }

    public List<Pessoa> getMulheres() {
        List<Pessoa> tmp = new ArrayList<Pessoa>();
        for (Pessoa p : pessoas) {
            if (p.getSexo() == 'F') {
                tmp.add(p);
            }
        }
        return tmp;
    }

    public List<Pessoa> getTodos() {
        return pessoas;
    }

    public Pessoa getPessoaPorNome(String n) {
        for(Pessoa p : pessoas) {
            if(p.getNome().equalsIgnoreCase(n))
                return p;
        }
        return null;
    }
}
