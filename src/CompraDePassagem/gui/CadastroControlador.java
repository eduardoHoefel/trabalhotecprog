/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package CompraDePassagem.gui;

import CompraDePassagem.dados.CadastroMem;
import CompraDePassagem.negocio.Cadastro;
import CompraDePassagem.negocio.Pessoa;
import CompraDePassagem.negocio.ValidadorPessoa;

import java.util.ArrayList;
import java.util.List;
import javax.swing.JTextArea;

/**
 *
 * @author Julio
 */
public class CadastroControlador {
    private Cadastro cadastro;
    private JTextArea saidaTexto;

    public CadastroControlador(JTextArea areaTexto) {
        cadastro = new CadastroMem();
        saidaTexto = areaTexto;
    }

    public boolean adicionarPessoa(String nome, boolean masculino) {
        if (ValidadorPessoa.validaNome(nome)) {
            Pessoa p = new Pessoa(nome, masculino);
            boolean adicionou = cadastro.adicionar(p);
            if(adicionou) {
                saidaTexto.append(p.toString() + "\r\n");
            }
            return adicionou;
        } else {
            return false;
        }
    }

    public List<String> buscarHomens() {
        List<String> retorno = new ArrayList<String>();
        for (Pessoa p : cadastro.getHomens()) {
            retorno.add(p.toString());
        }
        return retorno;
    }

    public List<String> buscarMulheres() {
        List<String> retorno = new ArrayList<String>();
        for (Pessoa p : cadastro.getMulheres()) {
            retorno.add(p.toString());
        }
        return retorno;
    }

    public List<String> getTodos() {
        List<String> retorno = new ArrayList<String>();
        for (Pessoa p : cadastro.getTodos()) {
            retorno.add(p.toString());
        }
        return retorno;

    }
}
